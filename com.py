# -*- coding: utf-8 -*-

import sys
import serial
import glob
from os import chdir
from ecg import *
from time import sleep
import threading

BAUDRATE      = 500000
SEND_INTERVAL = 10
s = None

ECGActif = True
VFCActif = False
firstRun = True

def listPorts():

	try:
		chdir('/dev/serial/by-id')
	except:
		print "No Serial Port... Retrying in 10 sec."
		sleep(10)
		return listPorts()

	ports = [file for file in glob.glob('*')]
	print ports
	portsOK = []

	for port in ports:
		try:
			print port
			s = serial.Serial('/dev/serial/by-id/' + port)
			s.close()
			portsOK.append(port)
		except (OSError, serial.SerialException):
			pass

	return portsOK

def setPort(ui, newPort):
	connect(ui, newPort)

def connect(ui, port):
	global s
	s = serial.Serial('/dev/serial/by-id/' + port[2:-2], BAUDRATE, timeout=0.05)
	ui.changeState('Connecting to ' + port)
	ui.update()
	sleep(0.2)
	if (s.is_open):
		ui.changeState('Connected to ' + port)
		ui.log(' Connected (Baudrate ' + str(BAUDRATE) + ')', 'INFO')
		ecg = ECG(ui)
		ecg.start()
		ui.ecgSpread(ecg)

		ui.after(2000, start, ui)
	else:
		ui.changeState(' Connection failed')

def start(ui):
	send(ui,'START')

def close():
	global s
	s.close()

def send(ui, text):
	global s
	s.write(text.encode())
	ui.log('>> ' + text, 'ECG')

def isConnected():
	global s
	if (s != None):
		if (s.is_open):
			return True
		else:
			return False
	else:
		return False

def read(ui):
	global s
	try:
		msg = s.readline()
		if (len(msg) != 0):
			if (msg.decode()[:5] == 'START' or msg.decode()[:4] == 'STOP') and not ui.debug.get():
				ui.log('<< ' + msg.decode(), 'PC')
			if ui.debug.get():
				ui.log('<< ' + msg.decode(), 'PC')
			return msg.decode()
		else:
			return None
	except (OSError, serial.SerialException):
		close()
		ui.changeState(' Connection lost')
		ui.log(' Not connected', 'INFO')

def sendECG(ui):
	global ECGActif
	if (ECGActif):
		send(ui, 'STOP DISPLAY')
	else:
		send(ui, 'START DISPLAY')
	ECGActif = not ECGActif
	
def sendVFC(ui):
	global VFCActif
	if (VFCActif):
		send(ui, 'STOP VFC')
	else:
		send(ui, 'START VFC')
	VFCActif = not VFCActif